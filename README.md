######################
Voice Filtering System
######################

The voice filtering system is a system which can recognize and filter a particular person�s voice from an audio stream. 
This system can be used in many occasions according to the requirement of the user though the system is designed to be used for 
the communication purposes.
The voice filtering system is a small device with a mic to take audio input and a 3.5mm audio socket to produce an audio output 
of the target voice. 
Users can plug ear phones to the system and attend to the target voice through it. 
The embedded system is supposed to take the audio input from the environment and then filter it from all the noises and unwanted 
sounds then output the target voice.

link to the Trello board.
https://trello.com/invite/b/xc1u4er7/c90a02c04a84b619dce87edbb7602e1e/voice-filtering-system